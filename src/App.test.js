import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should render a sign up form', () => {
  const wrapper = mount(<App />);
  const signUpForm = wrapper.find('#signup-form');

  expect(signUpForm.exists()).toEqual(true);
})
