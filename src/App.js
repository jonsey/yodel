import React from 'react';
import styled from 'styled-components';
import logo from './logo.png';
import './App.css';
import SignUp from './components/SignUp';


const SignUpWrapper = styled.div`
  background-color: #fff;
  width: 90%;
  margin: 3em auto;

  @media only screen and (min-width: 641px) {
    width: 50%;
  }
`;

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <SignUpWrapper>
        <SignUp />
      </SignUpWrapper>
    </div>
  );
}

export default App;
