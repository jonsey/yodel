module Pages.SchroedingerProvit exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)


view : Model -> Html msg
view model =
    let
        guessText guess =
            case guess of
                Just result ->
                    if result then
                        "True"
                    else
                        "False"

                Nothing ->
                    "Error"
    in
        div [ class "card provit-page" ]
            [ h1 [] [ text "Check any game result" ]
            , div [] [ text "The Transaction Id from the Blockchain of the guess is used as the seed to a random number generator, which gives the result of the game." ]
            , div [ class "provit__input-row" ]
                [ label [] [ text "Transaction ID" ]
                , input
                    [ class "provit__username"
                    , type_ "text"
                    , value model.provitTransactionId
                    ]
                    []
                ]
            , div [ class "provit__input-row" ]
                [ label [] [ text "Guess" ]
                , input
                    [ class "provit__username"
                    , type_ "text"
                    , value <| guessText model.provitGuess
                    ]
                    []
                ]
            , div [ class "provit__input-row" ]
                [ a [ href "https://repl.it/@Jonsey/SparklingDisfiguredCookie", target "_blank" ]
                    [ text "Click here to run the proof for yourself on Repl" ]
                ]
            ]
