const SUCCESS_MESSAGE = 'Success, Thank you for registering.';
const EMAIL_ERROR = 'Invalid email format.';
const PASSWORD_ERROR = 'Password is invalid. Should be only numbers, not the same and not consecutively ascending or descending.';
const PASSWORD_CONFIRMATION_ERROR = 'Password confirmation does not match the password.';

export default {
  SUCCESS_MESSAGE,
  EMAIL_ERROR,
  PASSWORD_ERROR,
  PASSWORD_CONFIRMATION_ERROR,
}
