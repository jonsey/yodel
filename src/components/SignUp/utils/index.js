const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/g;

const isEmpty = test =>
  (test === undefined || test === null || test === '')

const emailIsNotValid = (email) => {
  const re = new RegExp(emailRegex);
  const matches = re.exec(email);

  return matches === null;
}

const isNotOnlyNumbers = test => {
  const re = new RegExp(/^[0-9]+/g);
  const matches = re.exec(test);

  return matches === null;
}

const isAllSameNumber = test => {

  for (var i = 0; i < test.length - 1; i++) {
    if (test[i] !== test[i+1]) {
      return false;
    }
  }
  return true;
}

const isConsecutiveAscending = test => {

  for (var i = 0; i < test.length - 1; i++) {
    if (+test[i] + 1 !== +test[i+1]) {
      return false;
    }
  }
  return true;
}

const isConsecutiveDescending = test => {

  for (var i = 0; i < test.length - 1; i++) {
    if (+test[i] - 1 !== +test[i+1]) {
      return false;
    }
  }
  return true;
}

const passwordIsNotValid = password => {

  return isNotOnlyNumbers(password)
    || isAllSameNumber(password)
    || isConsecutiveAscending(password)
    || isConsecutiveDescending(password);

}

export default {
  isEmpty,
  emailIsNotValid,
  passwordIsNotValid,
}
