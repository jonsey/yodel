import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import utils from './utils';
import CONSTANTS from './constants';

const Form = styled.form`
  border: solid 1px #b3b3b370;
  padding: 1em;
  display: flex;
  flex-direction: column;

  @media only screen and (min-width: 641px) {
    padding: 3em;
  }
`;

const FormRow = styled.div`
  padding: 1em;
  display: flex;
  flex-direction: column;

  label {
    text-align: left;
    margin-bottom: 0.3em;
  }

  input {
    line-height: 2em;
    height: 2em;
    border: 0;
    padding: 1em;
    background-color: #ececec;
    border-bottom: solid 1px #b3b3b3;
  }
`;

const Required = styled.span`
  color: red;
  padding-left: 0.2em;
`;

const Submit = styled.input`
  text-decoration: none;
  padding: 20px 52px 16px;
  min-width: 140px;
  min-height: 60px;
  background-color: #9c0;
  background-image: none;
  cursor: pointer;
  color: #000;
  display: inline-block;
  border: none;
  font-weight: 800;
  text-transform: uppercase;
  font-size: 19px;
  font-size: 1.9rem;
  line-height: 19px;
  line-height: 1.9rem;
  transition: color 200ms ease-out,border-color 200ms ease-out,background-color 200ms ease-out;

  :hover {
    color: #fff;
    text-decoration: none;
    background-color: #000;
  }
`;

const Success = styled.div`
  padding: 1em;
`;

const ErrorMessage = styled.div`
  text-align: left;
  color: red;
`;

function SignUp() {
  const [email, setEmail] = useState('');
  const [emailErrors, setEmailErrors] = useState('');
  const [password, setPassword] = useState('');
  const [passwordErrors, setPasswordErrors] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [passwordConfirmationErrors, setPasswordConfirmationErrors] = useState('');
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    if (!utils.isEmpty(email)) {
      setEmailErrors('');
    }
    if (!utils.isEmpty(password)) {
      setPasswordErrors('');
    }
    if (!utils.isEmpty(passwordConfirmation)) {
      setPasswordConfirmationErrors('');
    }
  }, [email, password, passwordConfirmation])

  const validateEmail = () => {
    if (utils.isEmpty(email)) {
      setEmailErrors('Email is required.');
      return false;
    } else if (utils.emailIsNotValid(email)) {
      setEmailErrors(CONSTANTS.EMAIL_ERROR);
      return false;
    } else {
      setEmailErrors('');
      return true;
    }
  }

  const validatePassword = () => {
    if (utils.isEmpty(password)) {
      setPasswordErrors('Password is required.');
      return false;
    } else if (utils.passwordIsNotValid(password)) {
      setPasswordErrors(CONSTANTS.PASSWORD_ERROR);
      return false;
    } else {
      setPasswordErrors('');
      return true;
    }
  }

  const validatePasswordConfirmation = () => {
    if (password !== passwordConfirmation) {
      setPasswordConfirmationErrors(CONSTANTS.PASSWORD_CONFIRMATION_ERROR);
      return false;
    } else {
      setPasswordConfirmationErrors('');
      return true;
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    const emailValid = validateEmail();
    const passwordValid = validatePassword();
    const confirmationValid = validatePasswordConfirmation();

    const isValid = emailValid && passwordValid && confirmationValid;

    if (isValid) {
      setSuccess(true);
    }
  }

  return success ?
    <Success aria-label='success-message'>{CONSTANTS.SUCCESS_MESSAGE}</Success> :
    <Form id='signup-form' aria-label='signup-form' noValidate={true} onSubmit={handleSubmit} method='post'>
      <h2>Register</h2>
      <FormRow>
        <label htmlFor='email-address'>Email<Required>*</Required></label>
        <input
          name='email-address'
          id='email-address'
          aria-label='email-address'
          autoComplete='email'
          type='email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <ErrorMessage aria-label="email-errors">
          {emailErrors}
        </ErrorMessage>
      </FormRow>
      <FormRow>
        <label htmlFor='password'>Password<Required>*</Required></label>
        <input
          name='password'
          aria-label='password'
          id='password'
          type='password'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <ErrorMessage aria-label="password-errors">
          {passwordErrors}
        </ErrorMessage>
      </FormRow>
      <FormRow>
        <label htmlFor='confirm-password'>Confirm Password<Required>*</Required></label>
        <input
          name='confirm-password'
          aria-label='confirm-password'
          id='confirm-password'
          type='password'
          value={passwordConfirmation}
          onChange={(e) => setPasswordConfirmation(e.target.value)}
        />
        <ErrorMessage aria-label="confirm-password-errors">
          {passwordConfirmationErrors}
        </ErrorMessage>
      </FormRow>
      <div className='validated-form__actions'>
        <Submit type="submit" value="Submit" />
      </div>
    </Form>
}

export default SignUp;
