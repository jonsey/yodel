import React from 'react';
import { shallow, mount } from 'enzyme';
import { cleanup, render, fireEvent, getByTestId} from "@testing-library/react";
import SignUp from './';

afterEach(cleanup);

describe('Signup Form fields', () => {

  it('should have an email field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('#email-address').exists()).toEqual(true);
  });

  it('should have a label for email field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('label[htmlFor="email-address"]').exists()).toEqual(true);
  });

  it('should have a password field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('#password').exists()).toEqual(true);
  });

  it('should have a label for password field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('label[htmlFor="password"]').exists()).toEqual(true);
  });

  it('should have a confirm password field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('#confirm-password').exists()).toEqual(true);
  });

  it('should have a label for confirm password field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('label[htmlFor="confirm-password"]').exists()).toEqual(true);
  });

  it('should have a label for confirm password field', () => {
    const wrapper = shallow(<SignUp />);
    expect(wrapper.find('label[htmlFor="confirm-password"]').exists()).toEqual(true);
  });

})

describe('Email vaildation', () => {

  it('should not be empty', () => {
    const wrapper = render(<SignUp />);

    const emailErrors = wrapper.getByLabelText('email-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.submit(form);

    expect(emailErrors.textContent).toBe('Email is required.')
  })

  it('should have a valid email address format', () => {
    const wrapper = render(<SignUp />);

    const email = wrapper.getByLabelText('email-address');
    const emailErrors = wrapper.getByLabelText('email-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(email, { target: { value: 'myemail' } })
    fireEvent.submit(form);

    expect(emailErrors.textContent).toBe('Invalid email format.')
  })
})

describe('Password vaildation', () => {

  it('should not be empty', () => {
    const wrapper = render(<SignUp />);

    const passwordErrors = wrapper.getByLabelText('password-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.submit(form);

    expect(passwordErrors.textContent).toBe('Password is required.')
  })

  it('should be all digits', () => {
    const wrapper = render(<SignUp />);

    const password = wrapper.getByLabelText('password');
    const passwordErrors = wrapper.getByLabelText('password-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(password, { target: { value: 'ABCeasyAs123' } })
    fireEvent.submit(form);

    expect(passwordErrors.textContent).toBe('Password is invalid. Should be only numbers, not the same and not consecutively ascending or descending.')
  })

  it('should not be all the same digit', () => {
    const wrapper = render(<SignUp />);

    const password = wrapper.getByLabelText('password');
    const passwordErrors = wrapper.getByLabelText('password-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(password, { target: { value: '1111111' } })
    fireEvent.submit(form);

    expect(passwordErrors.textContent).toBe('Password is invalid. Should be only numbers, not the same and not consecutively ascending or descending.')
  })

  it('should not be consecutive ascending', () => {
    const wrapper = render(<SignUp />);

    const password = wrapper.getByLabelText('password');
    const passwordErrors = wrapper.getByLabelText('password-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(password, { target: { value: '123456789' } })
    fireEvent.submit(form);

    expect(passwordErrors.textContent).toBe('Password is invalid. Should be only numbers, not the same and not consecutively ascending or descending.')
  })

  it('should not be consecutive descending', () => {
    const wrapper = render(<SignUp />);

    const password = wrapper.getByLabelText('password');
    const passwordErrors = wrapper.getByLabelText('password-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(password, { target: { value: '987654321' } })
    fireEvent.submit(form);

    expect(passwordErrors.textContent).toBe('Password is invalid. Should be only numbers, not the same and not consecutively ascending or descending.')
  })
})

describe('Password confirmation', () => {

  it('Should match the password', () => {
    const wrapper = render(<SignUp />);

    const password = wrapper.getByLabelText('password');
    const passwordConfirmation = wrapper.getByLabelText('confirm-password');
    const passwordErrors = wrapper.getByLabelText('confirm-password-errors');
    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(password, { target: { value: '19245243' } })
    fireEvent.change(passwordConfirmation, { target: { value: '34464' } })
    fireEvent.submit(form);

    expect(passwordErrors.textContent).toBe('Password confirmation does not match the password.')
  })

})

describe('Valid form', () => {

  it('Should display success message on valid submition', () => {
    const wrapper = render(<SignUp />);

    const email = wrapper.getByLabelText('email-address');
    const password = wrapper.getByLabelText('password');
    const passwordConfirmation = wrapper.getByLabelText('confirm-password');

    const form = wrapper.getByLabelText('signup-form');

    fireEvent.change(email, { target: { value: 'damian@example.com' } })
    fireEvent.change(password, { target: { value: '19245243' } })
    fireEvent.change(passwordConfirmation, { target: { value: '19245243' } })
    fireEvent.submit(form);

    const formSubmitted = wrapper.getByLabelText('success-message');

  })

})
